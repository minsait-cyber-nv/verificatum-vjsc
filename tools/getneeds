#!/bin/sh

# Copyright 2008-2016 Douglas Wikstrom
#
# This file is part of Verificatum JavaScript Cryptographic library
# (VJSC). VJSC is NOT free software. It is distributed under
# Verificatum License 1.0 and Verificatum License Appendix 1.0 for
# VJSC.
#
# You should have agreed to this license and appendix when
# downloading VJSC and received a copy of the license and appendix
# along with VJSC. If not, then the license and appendix are
# available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
# http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
#
# If you do not agree to the combination of this license and
# appendix, then you may not use VJSC in any way and you must delete
# VJSC immediately.

# Recursively expands all dependencies of the files in the root
# directory.

SCRIPTPATH=$(readlink -f "$0")
SCRIPTDIR=$(dirname "$SCRIPTPATH")

TMP_DIR=${1}
ROOT=${2}
NEEDS=${3}

GET_DIR=${TMP_DIR}/getneeds
mkdir -p ${GET_DIR}
EXP_DIR=${TMP_DIR}/expandneeds
mkdir -p ${EXP_DIR}

# Start with all files.
rm -f ${GET_DIR}/newneeds
touch ${GET_DIR}/newneeds
find ${ROOT} | grep "\.js" | sed "s|${ROOT}/||" > ${GET_DIR}/oldneeds

# Expand.
DIFF=1
while !( test ${DIFF} = 0 );
do
    ${SCRIPTDIR}/expandneeds ${EXP_DIR} ${ROOT} ${GET_DIR}/oldneeds ${GET_DIR}/newneeds

    # Swap oldneeds and newneeds
    mv ${GET_DIR}/oldneeds ${GET_DIR}/tmpneeds
    mv ${GET_DIR}/newneeds ${GET_DIR}/oldneeds
    mv ${GET_DIR}/tmpneeds ${GET_DIR}/newneeds

    DIFF=`diff ${GET_DIR}/newneeds ${GET_DIR}/oldneeds | wc -l`
done

mv ${GET_DIR}/oldneeds ${NEEDS}

rm -rf ${GET_DIR} ${EXP_DIR}
