#!/bin/sh

# Copyright 2008-2016 Douglas Wikstrom
#
# This file is part of Verificatum JavaScript Cryptographic library
# (VJSC). VJSC is NOT free software. It is distributed under
# Verificatum License 1.0 and Verificatum License Appendix 1.0 for
# VJSC.
#
# You should have agreed to this license and appendix when
# downloading VJSC and received a copy of the license and appendix
# along with VJSC. If not, then the license and appendix are
# available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
# http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
#
# If you do not agree to the combination of this license and
# appendix, then you may not use VJSC in any way and you must delete
# VJSC immediately.

add_result() {

    TOOLNAME=$1
    CONTENTFILE=$2
    OUTPUTFILE=$3

    printf "\n#########################################################\n" \
>> $OUTPUTFILE
    printf "$TOOLNAME\n\n" >> $OUTPUTFILE

    CONTENT=`cat $CONTENTFILE`

    if test "x$CONTENT" = x;
    then
        printf "NO COMPLAINTS!\n" >> $OUTPUTFILE
    else
        printf "%s" "$CONTENT" >> $OUTPUTFILE
    fi
}

printf "\nCODE ANALYSIS REPORTS\n" > analysis_report.txt
add_result "JSHint (configured using jshint_conf.json)" jshint/jshint_report.txt analysis_report.txt
add_result "ESLint (configured using eslintrc.js)" eslint/eslint_report.txt analysis_report.txt

printf "\n"
