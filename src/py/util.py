
# Copyright 2008-2016 Douglas Wikstrom
#
# This file is part of Verificatum JavaScript Cryptographic library
# (VJSC). VJSC is NOT free software. It is distributed under
# Verificatum License 1.0 and Verificatum License Appendix 1.0 for
# VJSC.
#
# You should have agreed to this license and appendix when
# downloading VJSC and received a copy of the license and appendix
# along with VJSC. If not, then the license and appendix are
# available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
# http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
#
# If you do not agree to the combination of this license and
# appendix, then you may not use VJSC in any way and you must delete
# VJSC immediately.

# Provides simple utility functions (syntactic sugar).

import time
import subprocess
import random

def evjs(command):
    """Wrapper for the script that allows evaluating our Javascript code
    from Python in a simple way.
    """
    return subprocess.Popen("nodejs js/evjs.js \"%s\"" % command,
                            shell=True,
                            stdout=subprocess.PIPE).stdout.read();

def h(x):
    """Convert an integer into a raw hexadecimal representation."""
    return hex(x).replace("0x", "").replace("L", "").upper()

def now():
    """Returns the epoch."""
    return int(time.time())

def randomHexString(byte_length, seed):
    """Returns a random string of hexadecimal characters of the given
    length.
    """
    random.seed(seed)

    s = ""
    for j in range(0, byte_length):
        s += h(random.randrange(0, 16))
    return s

def randomInt(bit_length, seed):
    """Returns a random integer."""
    s = randomHexString((bit_length + 3) / 4, seed)
    return int(s, 16) % (1 << bit_length)
