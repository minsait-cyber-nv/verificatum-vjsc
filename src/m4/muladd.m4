dnl Copyright 2008-2016 Douglas Wikstrom
dnl
dnl This file is part of the Verificatum JavaScript Cryptography
dnl library (VJSC).
dnl
dnl VJSC is NOT free software. It is distributed under the
dnl Verificatum License 1.0. You should have agreed to this license
dnl when downloading VJSC and received a copy of the license along
dnl with VJSC. If not, then the license is available at
dnl <http://www.verificatum.com/VERIFICATUM_LICENSE_1.0>. If you do not
dnl agree to this license, then you may not use VJSC in any way and
dnl you must delete VJSC immediately.
dnl define([[[[[M4_LONG_ADD_WORDS]]]]],[[[[[dnl

define([[[[[M4_LONG_ADD_WORDS]]]]],[[[[[dnl
\$5 = (\$1|0) + (\$3|0);
\$1 = \$5 & M4_MASK_ALL;
\$2 = ((\$2|0) + (\$4|0) + (\$5 >>> M4_WORDSIZE)) & M4_MASK_ALL]]]]])dnl
dnl define([[[[[M4_LONG_ADD_WORDS]]]]],[[[[[dnl
dnl \$5 = \$1 + \$3;
dnl \$1 = \$5 & M4_MASK_ALL;
dnl \$2 = (\$2 + \$4 + (\$5 >>> M4_WORDSIZE)) & M4_MASK_ALL]]]]])dnl
define([[[[[M4_LONG_ADD]]]]],dnl
[[[[[M4_LONG_ADD_WORDS(\$1[0],\$1[1],\$2[0],\$2[1],\$3)]]]]])dnl
define([[[[[M4_MUL_WORD]]]]],[[[[[dnl
\$4[0] = \$2;
\$1[0] = 0;
\$1[1] = muladd_loop(\$1, \$4, 0, 1, \$3, 0, 0)dnl
]]]]])dnl
dnl \$5 = \$1 + \$3;
dnl \$1 = \$5 & M4_MASK_ALL;
dnl \$2 = (\$2 + \$4 + (\$5 >>> M4_WORDSIZE)) & M4_MASK_ALL]]]]])dnl
