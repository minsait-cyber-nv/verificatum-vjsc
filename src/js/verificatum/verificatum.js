
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

// ######################################################################
// ############## Javascript Verificatum Crypto Libary ##################
// ######################################################################

M4_NEEDS(verificatum/arithm/arithm.js)dnl
M4_NEEDS(verificatum/crypto/crypto.js)dnl

/**
 * @description
M4_INCLUDE(verificatum/README.js)dnl
 * @namespace verificatum
 */
var verificatum = (function () {

dnl Utility library.
M4_INCLUDE(verificatum/util/util.js)dnl

dnl Extended input/output library.
M4_INCLUDE(verificatum/eio/eio.js)dnl

dnl Library for arithmetic objects.
M4_INCLUDE(verificatum/arithm/arithm.js)dnl

dnl Library for cryptography.
M4_INCLUDE(verificatum/crypto/crypto.js)dnl

dnl Library for benchmarking.
M4_INCLUDEOPT(verificatum/benchmark/benchmark.js)dnl

    return {
        "version": "M4_VJSC_VERSION",

        "util": util,
        "eio": eio,
        "arithm": arithm,
        "crypto": crypto,
M4_EXPOPT(verificatum/benchmark/benchmark.js,benchmark)
    };
})();
