
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

ModPGroup.named_groups = {

    // RFC 2409, RFC 2412, RFC 3526
    M4_INCLUDEOPT(verificatum/arithm/ModPGroup_named_modp768.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ModPGroup_named_modp1024.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ModPGroup_named_modp1536.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ModPGroup_named_modp2048.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ModPGroup_named_modp3072.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ModPGroup_named_modp4096.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ModPGroup_named_modp6144.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ModPGroup_named_modp8192.js)dnl
};
