
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

// ######################################################################
// ################### Hom ##############################################
// ######################################################################

M4_NEEDS(verificatum/arithm/PField.js)dnl
M4_NEEDS(verificatum/arithm/PGroup.js)dnl

/* jshint -W098 */ /* Ignore unused. */
/* eslint-disable no-unused-vars */
/**
 * @description Homomorphism from a ring to a group.
 * @param domain Domain of homomorphism.
 * @param range Range of homomorphism.
 * @class
 * @abstract
 * @memberof verificatum.arithm
 */
function Hom(domain, range) {
    this.domain = domain;
    this.range = range;
}
Hom.prototype = Object.create(Object.prototype);
Hom.prototype.constructor = Hom;

/**
 * @description Evaluates the homomorphism.
 * @param value Input to the homomorphism.
 * @return Value of the homomorphism at the given value.
 * @method
 */
Hom.prototype.eva = function (value) {
    throw new Error("Abstract method!");
};
/* jshint +W098 */ /* Stop ignoring unused. */
/* eslint-enable no-unused-vars */
