
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

// ######################################################################
// ################### Test arithm.js ###################################
// ######################################################################

M4_INCLUDE(verificatum/verificatum.js)dnl
M4_INCLUDE(verificatum/dev/dev.js)dnl

var test_arithm = (function () {
    var test = verificatum.dev.test;
    var arithm = verificatum.arithm;
    var randomSource = new verificatum.crypto.RandomDevice();
    var statDist = 50;

dnl Lists of primes.
M4_INCLUDE(verificatum/arithm/test_primes.js)dnl

dnl Test li.
M4_INCLUDEOPT(verificatum/arithm/test_li.js)dnl

dnl Test LargeInteger.
M4_INCLUDEOPT(verificatum/arithm/test_LargeInteger.js)dnl

dnl Test ModPowProd.
M4_INCLUDEOPT(verificatum/arithm/test_ModPowProd.js)dnl

dnl Test FixModPow.
M4_INCLUDEOPT(verificatum/arithm/test_FixModPow.js)dnl

dnl Generic tests for subclasses of PRing.
M4_INCLUDEOPT(verificatum/arithm/test_PRing.js)dnl

dnl Tests for PField.
M4_INCLUDEOPT(verificatum/arithm/test_PField.js)dnl

dnl Tests for PPRing.
M4_INCLUDEOPT(verificatum/arithm/test_PPRing.js)dnl

dnl Generic tests for PGroup.
M4_INCLUDEOPT(verificatum/arithm/test_PGroup.js)dnl

dnl Tests for ModPGroup.
M4_INCLUDEOPT(verificatum/arithm/test_ModPGroup.js)dnl

dnl Tests for ECqPGroup.
M4_INCLUDEOPT(verificatum/arithm/test_ECqPGroup.js)dnl

dnl Tests for PPGroup.
M4_INCLUDEOPT(verificatum/arithm/test_PPGroup.js)dnl

    var run = function (testTime) {
        test.startSet("verificatum/arithm/");

// This is mainly used for debugging, and only makes sense to run
// during development. It does exhaustive testing for every input in
// some functions, which takes a lot of time.
M4_RUNOPT(verificatum/arithm/test_li.js,test_li,testTime)

M4_RUNOPT(verificatum/arithm/test_LargeInteger.js,test_LargeInteger,testTime)
M4_RUNOPT(verificatum/arithm/test_ModPowProd.js,test_ModPowProd,testTime)
M4_RUNOPT(verificatum/arithm/test_FixModPow.js,test_FixModPow,testTime)
M4_RUNOPT(verificatum/arithm/test_PField.js,test_PField,testTime)
M4_RUNOPT(verificatum/arithm/test_PPRing.js,test_PPRing,testTime)
M4_RUNOPT(verificatum/arithm/test_ModPGroup.js,test_ModPGroup,testTime)
M4_RUNOPT(verificatum/arithm/test_ECqPGroup.js,test_ECqPGroup,testTime)
M4_RUNOPT(verificatum/arithm/test_PPGroup.js,test_PPGroup,testTime)
    };

    return {
M4_EXPOPT(verificatum/arithm/test_li.js,test_li.js,test_li)
M4_EXPOPT(verificatum/arithm/test_LargeInteger.js,test_LargeInteger,test_LargeInteger)
M4_EXPOPT(verificatum/arithm/test_ModPowProd.js,test_ModPowProd)
M4_EXPOPT(verificatum/arithm/test_FixModPow.js,test_FixModPow)
M4_EXPOPT(verificatum/arithm/test_PField.js,test_PField)
M4_EXPOPT(verificatum/arithm/test_PPRing.js,test_PPRing)
M4_EXPOPT(verificatum/arithm/test_ModPGroup.js,test_ModPGroup)
M4_EXPOPT(verificatum/arithm/test_ECqPGroup.js,test_ECqPGroup)
M4_EXPOPT(verificatum/arithm/test_PPGroup.js,test_PPGroup)
        run: run
    };
})();
