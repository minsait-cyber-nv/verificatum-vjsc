
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

// ######################################################################
// ################### ExpHom ###########################################
// ######################################################################

M4_NEEDS(verificatum/arithm/Hom.js)dnl

/**
 * @description Exponentiation homomorphism from a ring to a
 * group. Note that the group is not necessarily a prime order group,
 * that the ring is not necessarily a field, and that the ring is not
 * necessarily the ring of exponents of group.
 * @param basis Basis element that is exponentiated.
 * @param domain Domain of homomorphism, which may be a subring of the
 * ring of exponents of the basis element.
 * @class
 * @abstract
 * @memberof verificatum.arithm
 */
function ExpHom(domain, basis) {
    Hom.call(this, domain, basis.pGroup);
    this.basis = basis;
}
ExpHom.prototype = Object.create(Hom.prototype);
ExpHom.prototype.constructor = ExpHom;

ExpHom.prototype.eva = function (value) {
    return this.basis.exp(value);
};
