
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

// ######################################################################
// ################### arithm ###########################################
// ######################################################################

/**
 * @description Arithmetic objects and routines. This is a port of the
 * Verificatum Mix-Net (VMN) which introduces abstractions that
 * facilitates the implementation of generalized cryptographic
 * primitives and protocols.
 *
 * <p>
 *
 * More precisely, the implementations of generalized primitives and
 * protocols is syntactically identical to their original versions,
 * e.g., the complex code found in other libraries for handling lists
 * of ciphertexts is completely eliminated. This gives less error
 * prone code, a smaller code base, and the code is easier to verify.
 *
 * @namespace arithm
 * @memberof verificatum
 */
var arithm = (function () {

dnl Root abstract class of arithmetic objects.
M4_INCLUDE(verificatum/arithm/ArithmObject.js)dnl

dnl Basic functionality for large integer arithmetic.
M4_INCLUDE(verificatum/arithm/li.js)dnl

dnl Basic functionality for large signed integer arithmetic.
M4_INCLUDE(verificatum/arithm/sli.js)dnl

dnl Large integer arithmetic.
M4_INCLUDE(verificatum/arithm/LargeInteger.js)dnl

dnl Simultaneous modular exponentiation.
M4_INCLUDEOPT(verificatum/arithm/ModPowProd.js)dnl

dnl Fixed-basis modular exponentiation.
M4_INCLUDEOPT(verificatum/arithm/FixModPow.js)dnl

dnl Prime order fields and product rings.
M4_INCLUDEOPT(verificatum/arithm/PRing.js)dnl

dnl Product ring.
M4_INCLUDEOPT(verificatum/arithm/PPRing.js)dnl

dnl Prime order field.
M4_INCLUDEOPT(verificatum/arithm/PField.js)dnl

dnl Basic elliptic curve arithmetic.
M4_INCLUDEOPT(verificatum/arithm/ec.js)dnl

dnl Abstract group where each non-trivial element has a given prime order.
M4_INCLUDEOPT(verificatum/arithm/PGroup.js)dnl

dnl Prime order subgroups of multiplicative groups modulo primes.
M4_INCLUDEOPT(verificatum/arithm/ModPGroup.js)dnl

dnl Elliptic curve groups over prime order fields.
M4_INCLUDEOPT(verificatum/arithm/ECqPGroup.js)dnl

dnl Product group.
M4_INCLUDEOPT(verificatum/arithm/PPGroup.js)dnl

dnl Homomorphism from ring to group.
M4_INCLUDEOPT(verificatum/arithm/Hom.js)dnl

dnl Exponentiation homomorphism from ring to group.
M4_INCLUDEOPT(verificatum/arithm/ExpHom.js)dnl

    // We only expose top-level objects. All elements of rings and
    // groups are instantiated through their container ring/group to
    // increase robustness.
    return {
        "li": li,
        "sli": sli,
        "LargeInteger": LargeInteger,
M4_EXPOPT(verificatum/arithm/ModPowProd.js,ModPowProd)
M4_EXPOPT(verificatum/arithm/FixModPow.js,FixModPow)
M4_EXPOPT(verificatum/arithm/PRing.js,PRing)
M4_EXPOPT(verificatum/arithm/PField.js,PField)
M4_EXPOPT(verificatum/arithm/PPRing.js,PPRing)
M4_EXPOPT(verificatum/arithm/PGroup.js,PGroup)
M4_EXPOPT(verificatum/arithm/ModPGroup.js,ModPGroup)
M4_EXPOPT(verificatum/arithm/ec.js,ec)
M4_EXPOPT(verificatum/arithm/ECqPGroup.js,ECqPGroup)
M4_EXPOPT(verificatum/arithm/PPGroup.js,PPGroup)
M4_EXPOPT(verificatum/arithm/Hom.js,Hom)
M4_EXPOPT(verificatum/arithm/ExpHom.js,ExpHom)

    };
})();
