
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

// ######################################################################
// ################### Test ModPowProd.js ###############################
// ######################################################################

M4_NEEDS(verificatum/arithm/ModPowProd.js)dnl

var test_ModPowProd = (function () {
    var prefix = "verificatum.arithm.ModPowProd";

dnl Primes.
M4_INCLUDE(verificatum/arithm/test_primes.js)dnl

    var fast_equal_naive = function (testTime) {
        var e;
        var i;
        var end = test.start([prefix + " (agrees with naive)"], testTime);

        var modulus = new arithm.LargeInteger(safe_primes[0]);

        var maxWidth = 10;
        var s = 100;
        while (!test.done(end)) {

            for (var width = 1; width <= maxWidth; width++) {

                var bases = [];
                for (i = 0; i < width; i++) {
                    bases[i] =
                        arithm.LargeInteger.INSECURErandom(modulus.bitLength());
                    bases[i] = bases[i].mod(modulus);
                }

                var exponents = [];
                for (i = 0; i < width; i++) {
                    var len = Math.max(1, modulus.bitLength() - 5 + i);
                    exponents[i] = arithm.LargeInteger.INSECURErandom(len);
                }

                var mpp = new arithm.ModPowProd(bases, modulus);
                var a = mpp.modPowProd(exponents);
                var b = arithm.ModPowProd.naive(bases, exponents, modulus);

                if (!a.equals(b)) {
                    e = "Modular power products disagrees!"
                        + "\nwidth = " + width
                        + "\na = " + a.toHexString()
                        + "\nb = " + b.toHexString();
                    test.error(e);
                }
            }
        }
        test.end();
    };

    var run = function (testTime) {
        fast_equal_naive(testTime);
    };
    return {run: run};
})();
