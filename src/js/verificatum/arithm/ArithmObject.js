
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

// ######################################################################
// ################### ArithmObject #####################################
// ######################################################################

/* istanbul ignore next */
/**
 * @description Arithmetic object.
 * @abstract
 * @class
 * @memberof verificatum.crypto
 */
function ArithmObject() {
};
ArithmObject.prototype = Object.create(Object.prototype);
ArithmObject.prototype.constructor = ArithmObject;

ArithmObject.prototype.getName = function () {
    var regex = /function\s?([^\(]{1,})\(/;
    var results = regex.exec(this.constructor.toString());
    return results && results.length > 1 ? results[1] : "";
};
