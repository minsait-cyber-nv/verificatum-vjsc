
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

// ######################################################################
// ################### ModPowProd ########################################
// ######################################################################

M4_NEEDS(verificatum/arithm/LargeInteger.js)dnl

/**
 * @description Pre-computes values to be used for simultaneous
 * exponentiation for a given list b of k bases and a modulus m. The
 * method {@link verificatum.arithm.ModPowProd#modPowProd} then takes
 * a list of exponents e and outputs the modular power product
 *
 * <p>
 *
 * g[0] ^ e[0] * ... * g[k - 1] ^ e[k - 1] mod m.
 *
 * <p>
 *
 * The number of exponents must match the number of bases for which
 * pre-computation is performed.
 *
 * @param bases List of bases.
 * @param modulus Modulus.
 * @class
 * @memberof verificatum.arithm
 */
function ModPowProd(bases, modulus) {

    var b = [];
    for (var i = 0; i < bases.length; i++) {
        b[i] = bases[i].value;
    }

    this.width = bases.length;
    this.t = li.modpowprodtab(b, modulus.value);
    this.modulus = modulus;
};

/**
 * @description Computes a power-product using the given exponents.
 * @param exponents Exponents.
 * @return Power product.
 * @method
 */
ModPowProd.prototype.modPowProd = function (exponents) {

    if (exponents.length !== this.width) {
        /* istanbul ignore next */
        throw Error("Wrong number of exponents! (" +
                    exponents.length + " != " + this.width + ")");
    }

    var e = [];
    for (var i = 0; i < exponents.length; i++) {
        e[i] = exponents[i].value;
    }

    var res = new LargeInteger(this.modulus.length);
    li.modpowprod(res.value, this.t, e, this.modulus.value);

    if (li.iszero(res.value)) {
        res.sign = 0;
    } else {
        res.sign = 1;
    }
    li.normalize(res.value);
    return res;
};

/**
 * @description Compute a power-product using the given bases,
 * exponents, and modulus. This is a naive implementation for simple
 * use and to debug {@link verificatum.arithm.ModPowProd#modPowProd}.
 * @param bases Bases.
 * @param exponents Exponents.
 * @param modulus Modulus.
 * @return Power product.
 * @method
 */
ModPowProd.naive = function (bases, exponents, modulus) {
    var result = LargeInteger.ONE;
    for (var i = 0; i < bases.length; i++) {
        result = result.modMul(bases[i].modPow(exponents[i], modulus), modulus);
    }
    return result;
};
