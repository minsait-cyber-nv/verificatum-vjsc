
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

// ######################################################################
// ################### Test FixModPow.js ###############################
// ######################################################################

M4_NEEDS(verificatum/arithm/FixModPow.js)dnl

var test_FixModPow = (function () {
    var prefix = "verificatum.arithm.FixModPow";

dnl Primes.
M4_INCLUDE(verificatum/arithm/test_primes.js)dnl

    var fast_equal_naive = function (testTime) {
        var e;
        var i;
        var end = test.start([prefix + " (agrees with generic)"], testTime);

        var modulus = new arithm.LargeInteger(safe_primes[0]);

        var maxWidth = 10;
        var s = 100;
        while (!test.done(end)) {

            var basis = arithm.LargeInteger.INSECURErandom(modulus.bitLength());

            for (var width = 1; width <= 8; width++) {

                var fmp = new arithm.FixModPow(basis, modulus, 20, width);

                for (var i = 1; i < modulus.bitLength() + 5; i++) {

                    var exponent = arithm.LargeInteger.INSECURErandom(i);

                    var exponents = fmp.slice(exponent);

                    var b = fmp.modPow(exponent);
                    var c = basis.modPow(exponent, modulus);

                    if (!b.equals(c)) {
                        e = "Fixed-base exponentiation is wrong!"
                            + "\nb = " + b.toHexString()
                            + "\nc = " + c.toHexString();
                        test.error(e);
                    }
                }
            }
        }
        test.end();
    };

    var run = function (testTime) {
        fast_equal_naive(testTime);
    };
    return {run: run};
})();
