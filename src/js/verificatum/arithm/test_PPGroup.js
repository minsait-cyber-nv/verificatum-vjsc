
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

// ######################################################################
// ################### Test PPGroup.js ##################################
// ######################################################################

M4_NEEDS(verificatum/arithm/PPGroup.js)dnl
M4_NEEDS(verificatum/arithm/test_PGroup.js)dnl

var test_PPGroup = (function () {
    var prefix = "verificatum.arithm.PPGroup";
    var test = verificatum.dev.test;

    var getPPGroups = function () {
        var tmp;

        var smallPGroups = test.getSmallPGroups();

        // Generate some PPGroups of different types.
        var pPGroups = [];
        for (var j = 0; j < smallPGroups.length; j++) {
            var flatPPGroup =
                new verificatum.arithm.PPGroup([smallPGroups[j],
                                                smallPGroups[j],
                                                smallPGroups[j]]);
            pPGroups.push(flatPPGroup);

            tmp = new verificatum.arithm.PPGroup([smallPGroups[j],
                                                  smallPGroups[j]]);
            tmp = new verificatum.arithm.PPGroup([smallPGroups[j], tmp]);
            var compPPGroup = new verificatum.arithm.PPGroup([tmp,
                                                              smallPGroups[j],
                                                              smallPGroups[j]]);
            pPGroups.push(compPPGroup);
        }
        return pPGroups;
    };

    var pPGroups = getPPGroups();

    var identities = function (testTime) {
        test_PGroup.identities(prefix, pPGroups, testTime);
    };
    var multiplication_commutativity = function (testTime) {
        test_PGroup.multiplication_commutativity(prefix, pPGroups, testTime);
    };
    var multiplication_associativity = function (testTime) {
        test_PGroup.multiplication_associativity(prefix, pPGroups, testTime);
    };
    var exp = function (testTime) {
        test_PGroup.exp(prefix, pPGroups, testTime);
    };
    var inversion = function (testTime) {
        test_PGroup.inversion(prefix, pPGroups, testTime);
    };
    var conversion = function (testTime) {
        test_PGroup.conversion(prefix, pPGroups, testTime);
    };
    var encoding = function (testTime) {
        test_PGroup.encoding(prefix, pPGroups, testTime);
    };
    var projprodgroup = function (testTime) {
        var end = test.start([prefix + " (proj and prod group)"], testTime);

        var i = 0;
        while (!test.done(end) && i < pPGroups.length) {

            var pPGroup = pPGroups[i];

            var newPGroups = [];
            for (var j = 0; j < pPGroup.getWidth(); j++) {
                newPGroups[j] = pPGroup.project(j);
            }
            var newPPGroup = new verificatum.arithm.PPGroup(newPGroups);

            if (!newPPGroup.equals(pPGroup)) {
                var e = "Projecting to parts and taking product failed!";
                test.error(e);
            }
            i++;
        }
        test.end();
    };
    var projprodel = function (testTime) {
        var end = test.start([prefix + " (proj and prod element)"], testTime);

        var i = 0;
        while (!test.done(end) && i < pPGroups.length) {

            var x = pPGroups[i].randomElement(randomSource, statDist);
            var xs = [];
            for (var j = 0; j < x.pGroup.getWidth(); j++) {
                xs[j] = x.project(j);
            }
            var y = pPGroups[i].prod(xs);

            if (!y.equals(x)) {
                var e = "Projecting to parts and taking product failed!";
                test.error(e);
            }
            i++;
        }
        test.end();
    };

    var run = function (testTime) {
        identities(testTime);
        multiplication_commutativity(testTime);
        multiplication_associativity(testTime);
        exp(testTime);
        inversion(testTime);
        conversion(testTime);
        encoding(testTime);
        projprodgroup(testTime);
        projprodel(testTime);
    };
    return {run: run};
})();
