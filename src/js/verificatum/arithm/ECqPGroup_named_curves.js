
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

ECqPGroup.named_curves = {

    // NIST
    M4_INCLUDEOPT(verificatum/arithm/ECqPGroup_named_prime192v1.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ECqPGroup_named_prime192v2.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ECqPGroup_named_prime192v3.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ECqPGroup_named_prime256v1.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ECqPGroup_named_prime239v1.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ECqPGroup_named_prime239v3.js)dnl

    // SEC
    M4_INCLUDEOPT(verificatum/arithm/ECqPGroup_named_secp192k1.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ECqPGroup_named_secp192r1.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ECqPGroup_named_secp224k1.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ECqPGroup_named_secp224r1.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ECqPGroup_named_secp256k1.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ECqPGroup_named_secp256r1.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ECqPGroup_named_secp384r1.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ECqPGroup_named_secp521r1.js)dnl

    // TeleTrusT
    M4_INCLUDEOPT(verificatum/arithm/ECqPGroup_named_brainpoolp192r1.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ECqPGroup_named_brainpoolp224r1.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ECqPGroup_named_brainpoolp256r1.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ECqPGroup_named_brainpoolp320r1.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ECqPGroup_named_brainpoolp384r1.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ECqPGroup_named_brainpoolp512r1.js)dnl

    // X9.62
    M4_INCLUDEOPT(verificatum/arithm/ECqPGroup_named_P-192.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ECqPGroup_named_P-224.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ECqPGroup_named_P-256.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ECqPGroup_named_P-384.js)dnl
    M4_INCLUDEOPT(verificatum/arithm/ECqPGroup_named_P-521.js)dnl

};
