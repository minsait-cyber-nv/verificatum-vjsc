
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

// ######################################################################
// ################### Test ModPGroup.js ################################
// ######################################################################

M4_NEEDS(verificatum/arithm/ModPGroup.js)dnl
M4_NEEDS(verificatum/arithm/test_PGroup.js)dnl

var test_ModPGroup = (function () {
    var prefix = "verificatum.arithm.ModPGroup";
    var arithm = verificatum.arithm;

dnl Example groups.
M4_INCLUDE(verificatum/arithm/test_ModPGroup_params.js)dnl

    var pGroups = arithm.ModPGroup.getPGroups();

    var identities = function (testTime) {
        test_PGroup.identities(prefix, pGroups, testTime);
    };
    var multiplication_commutativity = function (testTime) {
        test_PGroup.multiplication_commutativity(prefix, pGroups, testTime);
    };
    var multiplication_associativity = function (testTime) {
        test_PGroup.multiplication_associativity(prefix, pGroups, testTime);
    };
    var exp = function (testTime) {
        test_PGroup.exp(prefix, pGroups, testTime);
    };
    var fixed = function (testTime) {
        test_PGroup.fixed(prefix, pGroups, testTime);
    };
    var inversion = function (testTime) {
        test_PGroup.inversion(prefix, pGroups, testTime);
    };
    var conversion = function (testTime) {
        test_PGroup.conversion(prefix, pGroups, testTime);
    };
    var encoding = function (testTime) {
        test_PGroup.encoding(prefix, pGroups, testTime);
    };
    var hex = function (testTime) {
        test_PGroup.hex(prefix, pGroups, testTime);
    };

    var run = function (testTime) {
        identities(testTime);
        multiplication_commutativity(testTime);
        multiplication_associativity(testTime);
        exp(testTime);
        fixed(testTime);
        inversion(testTime);
        conversion(testTime);
        encoding(testTime);
        hex(testTime);
    };
    return {
        pGroups: pGroups,
        run: run
    };
})();
