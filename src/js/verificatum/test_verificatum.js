
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

M4_NEEDS(verificatum/verificatum.js)dnl
M4_NEEDS(verificatum/dev/dev.js)dnl
M4_NEEDS(verificatum/arithm/test_arithm.js)dnl
M4_NEEDS(verificatum/crypto/test_crypto.js)dnl

// ######################################################################
// ################### Test verificatum.js ##############################
// ######################################################################

M4_INCLUDE(verificatum/verificatum.js)dnl
M4_INCLUDE(verificatum/dev/dev.js)dnl

dnl Test utility functions.
M4_INCLUDEOPT(verificatum/util/test_util.js)dnl

dnl Test extended input and output routines. 
M4_INCLUDEOPT(verificatum/eio/test_eio.js)dnl

dnl Test arithmetic.
M4_INCLUDEOPT(verificatum/arithm/test_arithm.js)dnl

dnl Test cryptography.
M4_INCLUDEOPT(verificatum/crypto/test_crypto.js)dnl

var test_verificatum = (function () {

    var run = function (testTime) {
M4_RUNOPT(verificatum/util/test_util.js,test_util,testTime)
M4_RUNOPT(verificatum/eio/test_eio.js,test_eio,testTime)
M4_RUNOPT(verificatum/arithm/test_arithm.js,test_arithm,testTime)
M4_RUNOPT(verificatum/crypto/test_crypto.js,test_crypto,testTime)
    };
    return {
M4_EXPOPT(verificatum/util/test_util.js,test_util)
M4_EXPOPT(verificatum/eio/test_eio.js,test_eio)
M4_EXPOPT(verificatum/arithm/test_arithm.js,test_arithm)
M4_EXPOPT(verificatum/crypto/test_crypto.js,test_crypto)
        run: run
    };
})();

var testTime = parseInt(process.argv[2]);

var startMessage =
    "\n"
    + "---------------------------------------------------------------------\n"
    + " RUNNING TESTS\n\n"
    + " Please be patient. Some tests take a long time to complete due to\n"
    + " how comprehensive they are. This is particularly true for some \n"
    + " cryptographic routines\n"
    + "---------------------------------------------------------------------";

console.log(startMessage);

test_verificatum.run(testTime);
