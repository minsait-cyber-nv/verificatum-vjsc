
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

/* istanbul ignore next */
/**
 * @description Tuning functionality.
 * @namespace tune
 * @memberof verificatum.dev
 */
var tune = (function () {

    var util = verificatum.util;
    var arithm = verificatum.arithm;

    /**
     * @description
     * @param ops Operations to perform at each threshold level.
     * @return
     * @function hex28
     * @memberof verificatum.arithm.li
     */
    var square_karatsuba = function (size) {
        var x;

        for (var wordLength = 10; wordLength < 100; wordLength++) {

            var ops = size / wordLength;

            var w = arithm.li.newarray(2 * wordLength);

            var start_naive = util.time_ms();
            for (var i = 0; i < ops; i++) {
                x = arithm.li.INSECURErandom(wordLength * arithm.li.WORDSIZE);
                arithm.li.square_naive(w, x);
            }
            var time_naive = util.time_ms() - start_naive;

            var start_karatsuba = util.time_ms();
            for (var i = 0; i < ops; i++) {
                x = arithm.li.INSECURErandom(wordLength * arithm.li.WORDSIZE);
                arithm.li.square_karatsuba(w, x, 0, wordLength);
            }
            var time_karatsuba = util.time_ms() - start_karatsuba;

            console.log("" + wordLength + " " + time_naive + " "
                        + time_karatsuba + " " + (time_naive / time_karatsuba));
        }
    }

    /**
     * @description
     * @param ops Operations to perform at each threshold level.
     * @return
     * @function hex28
     * @memberof verificatum.arithm.li
     */
    var mul_karatsuba = function (size) {
        var x;
        var y;

        for (var wordLength = 10; wordLength < 100; wordLength++) {

            var ops = size / wordLength;

            var w = arithm.li.newarray(2 * wordLength);

            var start_naive = util.time_ms();
            for (var i = 0; i < ops; i++) {
                x = arithm.li.INSECURErandom(wordLength * arithm.li.WORDSIZE);
                y = arithm.li.INSECURErandom(wordLength * arithm.li.WORDSIZE);
                arithm.li.mul_naive(w, x, y);
            }
            var time_naive = util.time_ms() - start_naive;

            var start_karatsuba = util.time_ms();
            for (var i = 0; i < ops; i++) {
                x = arithm.li.INSECURErandom(wordLength * arithm.li.WORDSIZE);
                y = arithm.li.INSECURErandom(wordLength * arithm.li.WORDSIZE);
                arithm.li.mul_karatsuba(w, x, y, 0, wordLength);
            }
            var time_karatsuba = util.time_ms() - start_karatsuba;

            console.log("" + wordLength + " " + time_naive + " "
                        + time_karatsuba + " " + (time_naive / time_karatsuba));
        }
    }

    return {
        "square_karatsuba": square_karatsuba,
        "mul_karatsuba": mul_karatsuba
    };
})();
