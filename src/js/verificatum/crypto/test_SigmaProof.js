
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

// ######################################################################
// ################### Test SigmaProof ##################################
// ######################################################################

M4_NEEDS(verificatum/crypto/SigmaProof.js)dnl

var test_SigmaProof = (function () {
    var util = verificatum.util;
    var test = verificatum.dev.test;

    var prove_and_verify = function (sp, instance, witness, hashfunction) {
        var e;

        // Verify that honestly computed proofs validate and that
        // slightly modified proofs do not.
        for (var j = 0; j < 5; j++) {
            label = randomSource.getBytes(j);

            var proof = sp.prove(label, instance, witness,
                                 hashfunction, randomSource, 50);
            if (!sp.verify(label, instance, hashfunction, proof)) {
                e = "Valid proof was rejected!"
                    + "\nlabel = " + util.byteArrayToHex(label)
                    + "\nwitness = " + witness.toString()
                    + "\ninstance = " + instance.toString()
                    + "\nproof = " + util.byteArrayToHex(proof);
                test.error(e);
                return false;
            }

            var rand = randomSource.getBytes(proof.length);
            var epsilon = randomSource.getBytes(proof.length);
            for (var l = 0; l < proof.length; l++) {

                if (rand[l] == 0) {
                    
                    // Introduce random single-bit errors in each byte.
                    var modproof = proof.slice();
                    modproof[l] ^= (1 << (epsilon[l] % 8));

                    if (sp.verify(label, instance, hashfunction, modproof)) {
                        e = "Invalid proof was accepted!"
                            + "\nlabel = " + util.byteArrayToHex(label)
                            + "\nwitness = " + witness.toString()
                            + "\ninstance = " + instance.toString()
                            + "\nproof = " + util.byteArrayToHex(proof);
                            + "\nmodproof = " + util.byteArrayToHex(modproof);
                        test.error(e);
                        return false;
                    }
                }
            }
        }
    }
    return {
        prove_and_verify: prove_and_verify
    };
})();
