
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

// ##################################################################
// ############### SHA256PRG ########################################
// ##################################################################

/**
 * @description Pseudo-random generator based on SHA-256 in counter
 * mode.
 * @class
 * @memberof verificatum.crypto
 */
function SHA256PRG() {
    this.input = null;
};
SHA256PRG.prototype = Object.create(RandomSource.prototype);
SHA256PRG.prototype.constructor = SHA256PRG;
SHA256PRG.seedLength = 32;

/**
 * @description Initializes this PRG with the given seed.
 * @param seed Seed bytes.
 * @method
 */
SHA256PRG.prototype.setSeed = function (seed) {
    if (seed.length >= 32) {
        this.input = seed.slice(0, 32);
        this.input.length += 4;
        this.counter = 0;
        this.buffer = [];
        this.index = 0;
    } else {
        throw Error("Too short seed!");
    }
};

SHA256PRG.prototype.getBytes = function (len) {
    if (this.input === null) {
        throw Error("Uninitialized PRG!");
    }

    var res = [];
    res.length = len;

    for (var i = 0; i < res.length; i++) {

        if (this.index === this.buffer.length) {
            verificatum.util.setUint32ToByteArray(this.input, this.counter, 32);
            this.buffer = sha256.hash(this.input);
            this.index = 0;
            this.counter++;
        }
        res[i] = this.buffer[this.index];
        this.index++;
    }
    return res;
};
