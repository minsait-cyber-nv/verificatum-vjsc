
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

// ######################################################################
// ################### SigmaProofAnd ####################################
// ######################################################################

M4_NEEDS(verificatum/crypto/SigmaProofPara.js)dnl

/**
 * @description Conjunction of Sigma proofs with identical challenge
 * spaces.
 *
 * @param sigmaProofs Component Sigma proofs.
 * @class
 * @extends verificatum.crypto.SigmaProofPara
 * @memberof verificatum.crypto
 */
function SigmaProofAnd(sigmaProofs) {
    SigmaProofPara.call(this, sigmaProofs);
}
SigmaProofAnd.prototype = Object.create(SigmaProofPara.prototype);
SigmaProofAnd.prototype.constructor = SigmaProofAnd;

SigmaProofAnd.prototype.randomnessByteLength = function (statDist) {
    var byteLength = 0;
    for (var i = 0; i < this.sigmaProofs.length; i++) {
        byteLength += this.sigmaProofs[i].randomnessByteLength(statDist);
    }
    return byteLength;
};

SigmaProofAnd.prototype.precompute = function (randomSource, statDist) {
    var precomputed = [];

    for (var i = 0; i < this.sigmaProofs.length; i++) {
        precomputed[i] = this.sigmaProofs[i].precompute(randomSource, statDist);
    }
    return precomputed;
};

SigmaProofAnd.prototype.commit = function (precomputed, instance, witness,
                                           randomSource, statDist) {
    var newPrecomputed = [];
    var commitment = [];
    for (var i = 0; i < this.sigmaProofs.length; i++) {
        var pair = this.sigmaProofs[i].commit(precomputed[i],
                                              instance[i], witness[i],
                                              randomSource, statDist);
        newPrecomputed[i] = pair[0];
        commitment[i] = pair[1];
    }
    return [newPrecomputed, commitment];
};

SigmaProofAnd.prototype.check = function (instance, commitment,
                                          challenge, reply) {
    var chall = util.fill(challenge, this.sigmaProofs.length);
    return SigmaProofPara.prototype.check.call(this,
                                               instance, commitment,
                                               chall, reply);
};

SigmaProofAnd.prototype.simulate = function (instance, challenge,
                                             randomSource, statDist) {
    var chall = util.fill(challenge, this.sigmaProofs.length);
    return SigmaProofPara.prototype.simulate.call(this,
                                                  instance, chall,
                                                  randomSource, statDist);
};
