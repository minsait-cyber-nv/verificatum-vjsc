
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

// ######################################################################
// ################### Test sha2.js #####################################
// ######################################################################

var test_sha2 = (function () {
    var prefix = "verificatum.crypto.sha2";
    var crypto = verificatum.crypto;
    var util = verificatum.util;
    var test = verificatum.dev.test;

dnl This is generated, so it is not in the source tree.
dnl Inputs and outputs of sha256.
M4_INCLUDE(verificatum/crypto/test_sha256_strings.js)dnl

    var hash_strings = function (testTime) {
        test.start([prefix + " (SHA-256)"], testTime);
        for (var i = 0; i < sha256_teststrings.length; i++) {
            var pair = sha256_teststrings[i];
            var bytes = util.asciiToByteArray(pair[0]);
            var md = crypto.sha256.hash(bytes);
            var mds = util.byteArrayToHex(md);

            if (mds !== pair[1]) {
                var e = "Input: " + pair[0] +
                    "\n" + mds +
                    "\n!= " + pair[1];
                throw Error(e);
            }
        }
        test.end();
    };
dnl
dnl NIST test vectors.
dnl    var hash_singleblock = function (testTime) {
dnl        var bytes = verificatum.util.asciiToByteArray("abc");
dnl        var digest = crypto.sha256.hash(bytes);
dnl    };
dnl    var hash_doubleblock = function (testTime) {
dnl        var s = "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq";
dnl        var bytes = verificatum.util.asciiToByteArray(s);
dnl        var digest = crypto.sha256.hash(bytes);
dnl    };

    var run = function (testTime) {
dnl        hash_singleblock(testTime);
dnl        hash_doubleblock(testTime);
        hash_strings(testTime);
    };
    return {run: run};
})();
