
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

// ######################################################################
// ################### Test SchnorrProof ################################
// ######################################################################

M4_NEEDS(verificatum/crypto/SchnorrProof.js)dnl

var test_SchnorrProof = (function () {
    var prefix = "verificatum.crypto.SchnorrProof";
    var arithm = verificatum.arithm;
    var crypto = verificatum.crypto;
    var util = verificatum.util;
    var test = verificatum.dev.test;

    var pGroups = test.getSmallPGroups();

    var prove_and_verify = function (testTime) {

        var end = test.start([prefix + " (prove and verify)"], testTime);

        while (!test.done(end)) {

            for (var i = 0; !test.done(end) && i < pGroups.length; i++) {

                var pGroup = pGroups[i];
                var eh;
                var sp;
                var witness;
                var instance;

                // eh(x) = g^x
                eh = new arithm.ExpHom(pGroup.pRing, pGroup.getg());
                sp = new crypto.SchnorrProof(eh);
                witness = eh.domain.randomElement(randomSource, statDist);
                instance = eh.eva(witness);

                test_SigmaProof.prove_and_verify(sp, instance, witness,
                                                 crypto.sha256);

                // pPGroup = pGroup x pGroup
                var pPGroup = new arithm.PPGroup([pGroup, pGroup]);

                // b = (c, d)
                var t = pGroup.pRing.randomElement(randomSource, statDist);
                var c = pGroup.getg().exp(t);

                var s = pGroup.pRing.randomElement(randomSource, statDist);
                var d = pGroup.getg().exp(s);

                var b = pPGroup.prod([c, d]);

                // eh(x) = (c^x, d^x)
                eh = new arithm.ExpHom(pGroup.pRing, b);
                sp = new crypto.SchnorrProof(eh);
                witness = eh.domain.randomElement(randomSource, statDist);
                instance = eh.eva(witness);

                test_SigmaProof.prove_and_verify(sp, instance, witness,
                                                 crypto.sha256);
            }
        }
        test.end();
    };

    var run = function (testTime) {
        prove_and_verify(testTime);
    };
    return {run: run};
})();
