
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

// ######################################################################
// ################### Test SigmaProofOr ################################
// ######################################################################

M4_NEEDS(verificatum/crypto/SigmaProofOr.js)dnl

var test_SigmaProofOr = (function () {
    var prefix = "verificatum.crypto.SigmaProofOr";
    var arithm = verificatum.arithm;
    var crypto = verificatum.crypto;
    var util = verificatum.util;
    var test = verificatum.dev.test;

    var pGroups = test.getSmallPGroups();

    var prove_and_verify = function (testTime) {

        var end = test.start([prefix + " (prove and verify)"], testTime);

        while (!test.done(end)) {

            for (var i = 0; !test.done(end) && i < pGroups.length; i++) {

                var pGroup = pGroups[i];
                var sp;
                var witness;
                var instance;

                // eh(x) = g^x
                sps = [];
                witness = [];
                instance = [];

                var instances = 3;
                for (var j = 0; j < instances; j++) {
                    eh = new arithm.ExpHom(pGroup.pRing, pGroup.getg());
                    sps[j] = new crypto.SchnorrProof(eh);
                    witness[j] =
                        eh.domain.randomElement(randomSource, statDist);
                    instance[j] = eh.eva(witness[j]);
                }
                var sp = new crypto.SigmaProofOr(pGroup.pRing.getPField(), sps);

                for (var j = 0; j < instances; j++) {
                    test_SigmaProof.prove_and_verify(sp, instance,
                                                     [witness[j], j],
                                                     crypto.sha256);

                }
            }
        }
        test.end();
    };

    var run = function (testTime) {
        prove_and_verify(testTime);
    };
    return {run: run};
})();
