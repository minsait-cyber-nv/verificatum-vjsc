
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

// ######################################################################
// ################### Test ElGamalZKPoKWriteIn.js ######################
// ######################################################################

M4_NEEDS(verificatum/crypto/ElGamalZKPoKWriteIn.js)dnl

var test_ElGamalZKPoKWriteIn = (function () {
    var prefix = "verificatum.crypto.ElGamalZKPoKWriteIn";
    var arithm = verificatum.arithm;
    var crypto = verificatum.crypto;
    var test = verificatum.dev.test;

    var gen_encrypt_decrypt = function (testTime) {
        var e;
        var end = test.start([prefix + " (encrypt and decrypt)"], testTime);

        var pGroups = test.getSmallPGroups();

        var maxKeyWidth = 3;
        var maxWidth = 4;

        var i = 1;
        while (!test.done(end)) {

            var keyWidth = 1;
            while (keyWidth <= maxKeyWidth) {

                var yGroup = arithm.PGroup.getWideGroup(pGroups[i], keyWidth);
                
                for (var l = 0; l < 2; l++) {

                    var ny = new crypto.ElGamalZKPoKWriteIn(l === 0,
                                                            yGroup,
                                                            crypto.sha256,
                                                            randomSource,
                                                            statDist);
                    var label = randomSource.getBytes(10)
                    var keys = ny.gen(yGroup);

                    var pk = keys[0];
                    var sk = keys[1];

                    var width = 1;
                    while (width <= maxWidth) {

                        var wpk = ny.widePublicKey(pk, width);
                        var wsk = ny.widePrivateKey(sk, width);

                        var m =
                            wpk.project(1).pGroup.randomElement(randomSource,
                                                                10);

                        var c = ny.encrypt(label, wpk, m);
                        var a = ny.decrypt(label, wpk, wsk, c);

                        if (a == null || !a.equals(m)) {
                            var e = "NaorYung failed!"
                                + "\npk = " + pk.toString()
                                + "\nsk = " + sk.toString()
                                + "\nkeyWidth = " + keyWidth
                                + "\nwpk = " + wpk.toString()
                                + "\nwsk = " + wsk.toString()
                                + "\nwidth = " + width
                                + "\nm = " + m.toString()
                                + "\nc = " + c.toString();
                            if (a != null) {
                                e += "\na = " + a.toString();
                            }
                            test.error(e);
                        }
                        width++;
                    }
                }
                keyWidth++;
            }
            i = (i + 1) % pGroups.length;
        }
        test.end();
    };


    var run = function (testTime) {
        gen_encrypt_decrypt(testTime);
    };
    return {run: run};
})();
