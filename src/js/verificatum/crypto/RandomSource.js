
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

// ##################################################################
// ############### RandomSource #####################################
// ##################################################################

/* jshint -W098 */ /* Ignore unused. */
/* eslint-disable no-unused-vars */
/**
 * @description Random source for cryptographic use.
 * @class
 * @memberof verificatum.crypto
 */
function RandomSource() {
};

/**
 * @description Generates the given number of random bytes.
 * @param len Number of bytes to generate.
 * @method
 */
RandomSource.prototype.getBytes = function (len) {
    throw new Error("Abstract method!");
};
/* jshint -W098 */ /* Stop ignoring unused. */
/* eslint-enable no-unused-vars */
