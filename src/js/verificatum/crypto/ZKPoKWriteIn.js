
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

// ######################################################################
// ################### ZKPoKWriteIn #####################################
// ######################################################################

M4_NEEDS(verificatum/crypto/ElGamalZKPoKAdapter.js)dnl
M4_NEEDS(verificatum/arithm/ExpHom.js)dnl
M4_NEEDS(verificatum/crypto/SchnorrProof.js)dnl

/**
 * @description Zero-knowledge proof needed to implement the Naor-Yung
 * cryptosystem.
 * @class
 * @extends verificatum.arithm.ZKPoK
 * @memberof verificatum.crypto
 */
function ZKPoKWriteIn(publicKey) {
    var domain = publicKey.project(1).pGroup.pRing;
    var basis = publicKey.project(0);
    var expHom = new arithm.ExpHom(domain, basis);
    this.sp = new SchnorrProof(expHom);
};
ZKPoKWriteIn.prototype = Object.create(ZKPoK.prototype);
ZKPoKWriteIn.prototype.constructor = ZKPoKWriteIn;

ZKPoKWriteIn.prototype.precompute = function (randomSource, statDist) {
    return this.sp.precompute(randomSource, statDist);
};

/**
 * @description Combines an arbitrary label with parts of the instance
 * not included as input by the ZKPoK itself.
 * @param label Label in the form of a byte array or byte tree.
 * @param instance Complete instance.
 * @return Combined label.
 */
ZKPoKWriteIn.makeLabel = function (label, instance) {
    var lbt = eio.ByteTree.asByteTree(label);
    var ebt = instance.project(1).toByteTree();
    return new eio.ByteTree([lbt, ebt]);
};

ZKPoKWriteIn.prototype.completeProof = function (precomputed,
                                            label, instance, witness,
                                            hashfunction,
                                            randomSource, statDist) {
    label = ZKPoKWriteIn.makeLabel(label, instance);
    return this.sp.completeProof(precomputed, label,
                                 instance.project(0), witness,
                                 hashfunction, randomSource, statDist);
};

ZKPoKWriteIn.prototype.verify = function (label, instance, hashfunction, proof) {
    label = ZKPoKWriteIn.makeLabel(label, instance);
    return this.sp.verify(label, instance.project(0), hashfunction, proof);
};
