
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

// ##################################################################
// ############### RandomDevice #####################################
// ##################################################################

/**
 * @description Random device for cryptographic use. This is a wrapper
 * of a built-in source of randomness that is different depending on
 * the platform. The definition depends on the platform, but
 * guarantees a random output secure for cryptographic use (assuming
 * that these libraries are correctly implemented).
 * @class
 * @memberof verificatum.crypto
 */
function RandomDevice() {
};
RandomDevice.prototype = Object.create(RandomSource.prototype);
RandomDevice.prototype.constructor = RandomDevice;

/* eslint-disable no-negated-condition */
// We are in a browser.
if (typeof window !== "undefined" && typeof window.crypto !== "undefined") {

    RandomDevice.prototype.getBytes = function (len) {
        var byteArray = new Uint8Array(len);
        window.crypto.getRandomValues(byteArray);
        var bytes = [];
        for (var i = 0; i < len; i++) {
            bytes[i] = byteArray[i];
        }
        return bytes;
    };

    // We are in nodejs.
} else if (typeof require !== "undefined") {

    RandomDevice.prototype.getBytes = (function () {
        var crypto = require("crypto");

        return function (len) {
            var tmp = crypto.randomBytes(len);
            var res = [];
            for (var i = 0; i < tmp.length; i++) {
                res[i] = tmp[i];
            }
            return res;
        };
    })();

    // We do not know where we are.
} else {
    RandomDevice.prototype.getBytes = (function () {
        return function () {
            throw Error("Unable to find a suitable random device!");
        };
    })();
}
/* eslint-enable no-negated-condition */
