
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

// ######################################################################
// ################### ElGamalZKPoKAdapter ##############################
// ######################################################################

M4_NEEDS(verificatum/crypto/ZKPoK.js)dnl

/* jshint -W098 */ /* Ignore unused. */
/* eslint-disable no-unused-vars */
/**
 * @description Adapter for {@link verificatum.crypto.ElGamalZKPoK}
 * that creates {@link verificatum.crypto.ZKPoK} that imposes
 * restrictions on plaintexts and ciphertexts.
 * @abstract
 * @class
 * @memberof verificatum.crypto
 */
function ElGamalZKPoKAdapter() {};
ElGamalZKPoKAdapter.prototype = Object.create(Object.prototype);
ElGamalZKPoKAdapter.prototype.constructor = ElGamalZKPoKAdapter;

/**
 * @description Generates a {@link verificatum.crypto.ZKPoK} that
 * imposes restrictions on ciphertexts.
 * @param publicKey El Gamal public key.
 * @return Instance of {@link verificatum.crypto.ZKPoK}.
 * @method
 */
ElGamalZKPoKAdapter.prototype.getZKPoK = function (publicKey) {
    throw new Error("Abstract method!");
};
/* jshint +W098 */ /* Stop ignoring unused. */
/* eslint-enable no-unused-vars */
