
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

// ######################################################################
// ################### Test crypto.js ###################################
// ######################################################################

M4_INCLUDE(verificatum/verificatum.js)dnl
M4_INCLUDE(verificatum/dev/dev.js)dnl

var test_crypto = (function () {
    var test = verificatum.dev.test;
    var crypto = verificatum.crypto;
    var randomSource = new verificatum.crypto.RandomDevice();
    var statDist = 50;

dnl Tests SHA-2.
M4_INCLUDE(verificatum/crypto/test_sha2.js)dnl

dnl Tests ElGamal.
M4_INCLUDEOPT(verificatum/crypto/test_ElGamal.js)dnl

dnl Tests Sigma proofs.
M4_INCLUDEOPT(verificatum/crypto/test_SigmaProof.js)dnl

dnl Tests conjunction of Sigma proofs.
M4_INCLUDEOPT(verificatum/crypto/test_SigmaProofAnd.js)dnl

dnl Tests disjunction of Sigma proofs.
M4_INCLUDEOPT(verificatum/crypto/test_SigmaProofOr.js)dnl

dnl Tests Schnorr proofs.
M4_INCLUDEOPT(verificatum/crypto/test_SchnorrProof.js)dnl

dnl Tests ElGamal with zero-knowledge proofs of knowledge.
M4_INCLUDEOPT(verificatum/crypto/test_ElGamalZKPoKWriteIn.js)dnl

    var run = function (testTime) {
        test.startSet("verificatum/crypto/");
        verificatum.dev.test.run(test_sha2, testTime);
M4_RUNOPT(verificatum/crypto/test_ElGamal.js,test_ElGamal,testTime)
M4_RUNOPT(verificatum/crypto/test_SchnorrProof.js,test_SchnorrProof,testTime)
M4_RUNOPT(verificatum/crypto/test_SigmaProofAnd.js,test_SigmaProofAnd,testTime)
M4_RUNOPT(verificatum/crypto/test_SigmaProofOr.js,test_SigmaProofOr,testTime)
M4_RUNOPT(verificatum/crypto/test_ElGamalZKPoKWriteIn.js,test_ElGamalZKPoKWriteIn,testTime)
    };
    return {
        test_sha2: test_sha2,
M4_EXPOPT(verificatum/crypto/test_ElGamal.js,test_ElGamal)
M4_EXPOPT(verificatum/crypto/test_SchnorrProof.js,test_SchnorrProof)
M4_EXPOPT(verificatum/crypto/test_SigmaProofAnd.js,test_SigmaProofAnd)
M4_EXPOPT(verificatum/crypto/test_SigmaProofOr.js,test_SigmaProofOr)
M4_EXPOPT(verificatum/crypto/test_ElGamalZKPoKWriteIn.js,test_ElGamalZKPoKWriteIn)
        run: run
    };
})();
