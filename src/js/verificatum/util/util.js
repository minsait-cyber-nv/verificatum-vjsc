
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

/**
 * @description Utility classes and functions.
 * @namespace util
 * @memberof verificatum
 */
var util = (function () {

dnl Utility functions.
M4_INCLUDE(verificatum/util/functions.js)dnl

    return {

        "time_ms": time_ms,
        "time": time,
        "ofType": ofType,
        "fill": fill,
        "full": full,
        "change_wordsize": change_wordsize,
        "asciiToByteArray": asciiToByteArray,
        "byteArrayToAscii": byteArrayToAscii,
        "byteArrayToHex": byteArrayToHex,
        "hexToByteArray": hexToByteArray,
        "equalsArray": equalsArray,
        "randomArray": randomArray,
        "readUint32FromByteArray": readUint32FromByteArray,
        "setUint32ToByteArray": setUint32ToByteArray,
        "readUint16FromByteArray": readUint16FromByteArray,
        "setUint16ToByteArray": setUint16ToByteArray
    };
})();
