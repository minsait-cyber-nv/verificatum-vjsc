
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

M4_INCLUDE(verificatum/verificatum.js)dnl
M4_INCLUDE(verificatum/dev/dev.js)dnl

// ##################################################################
// ############### Test util.js #####################################
// ##################################################################

var test_util = (function () {
    var test = verificatum.dev.test;
    var util = verificatum.util;
    var randomSource = new verificatum.crypto.RandomDevice();

    var byteArrayToFromHex = function (testTime) {
        var endEpoch =
            test.start(["verificatum.util.byteArrayToHex",
                        "verificatum.util.hexToByteArray"],
                       testTime);
        var simpleLen = 20;
        var len = 1;
        while (!test.done(endEpoch)) {

            for (var wordsize = 8; wordsize < 64; wordsize *= 2) {

                var x = util.randomArray(len, 8, randomSource);
                var hex = util.byteArrayToHex(x);
                var y = util.hexToByteArray(hex, wordsize);

                if (!util.equalsArray(x, y)) {
                    throw Error("Failed!")
                }
            }
            len = len % (simpleLen - 1) + 1;
        }
        test.end();
    };

    var byteArrayToFromAscii = function (testTime) {

        test.start(["verificatum.util.byteArrayToAscii",
                    "verificatum.util.asciiToByteArray"],
                   testTime);

        var bytes = [];
        for (var i = 0; i < 256; i++) {
            bytes[i] = i;
        }

        for (var i = 0; i < bytes.length; i++) {
            var ascii = util.byteArrayToAscii(bytes.slice(0, i));
            var bytes2 = util.asciiToByteArray(ascii);
            for (var j = 0; j < bytes2.length; j++) {
                if (bytes2[j] !== bytes[j]) {
                    throw Error("Failed!");
                }
            }
        }
        test.end();
    };

    var run = function (testTime) {
        test.startSet("verificatum/util/");
        byteArrayToFromHex(testTime);
        byteArrayToFromAscii(testTime);
    };
    return {run: run};
})();
