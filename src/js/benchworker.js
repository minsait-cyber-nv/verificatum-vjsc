
// Copyright 2008-2016 Douglas Wikstrom
//
// This file is part of Verificatum JavaScript Cryptographic library
// (VJSC). VJSC is NOT free software. It is distributed under
// Verificatum License 1.0 and Verificatum License Appendix 1.0 for
// VJSC.
//
// You should have agreed to this license and appendix when
// downloading VJSC and received a copy of the license and appendix
// along with VJSC. If not, then the license and appendix are
// available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
// http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VJSC
//
// If you do not agree to the combination of this license and
// appendix, then you may not use VJSC in any way and you must delete
// VJSC immediately.

var benchworker = function () {

var randomSource = null;
var statDist = 50;
var maxWidth = 4;

var crypto;
var benchmark;
var arithm;
var hashfunction;

var getPGroups = function (groupNames) {
    var pGroups = [];
    for (var i = 0; i < groupNames.length; i++) {
        pGroups[i] = arithm.PGroup.getPGroup(groupNames[i]);
    }
    return pGroups;
};

var getIndices = function (maxWidth) {
    var indices = [];
    for (var i = 0; i < maxWidth; i++) {
        indices[i] = i + 1;
    }
    return indices;
};
    
onmessage = function(e) {

    var command = e.data[0];

    if (command == "importScripts") {

        importScripts(e.data[1] + '/min-vjsc-M4_VJSC_VERSION.js');
        crypto = verificatum.crypto;
        benchmark = verificatum.benchmark;
        arithm = verificatum.arithm;
        hashfunction = crypto.sha256;
        randomSource = new crypto.SHA256PRG();
        randomSource.setSeed(e.data[2]);

        postMessage(["importScripts"]);

    } else {
        
        var minSamples = e.data[1];
        var params = e.data.slice(1);

        if (command == "ModPGroup.exp") {
            var pGroupNames = arithm.ModPGroup.getPGroupNames();
            var pGroups = arithm.ModPGroup.getPGroups();
            var results = arithm.PGroup.benchExp(pGroups,
                                                 minSamples,
                                                 randomSource);
            postMessage(["ModPGroup.exp",
                         benchmark.grpTable(pGroupNames, results)]);

        } else if (command == "ECqPGroup.exp") {
            var pGroupNames = arithm.ECqPGroup.getPGroupNames();
            var pGroups = arithm.ECqPGroup.getPGroups();
            var results = arithm.PGroup.benchExp(pGroups,
                                                 minSamples,
                                                 randomSource);
            postMessage(["ECqPGroup.exp",
                         benchmark.grpTable(pGroupNames, results)]);

        } else if (command == "FixModPow.exp") {
            var pGroupNames = ["modp3072", "modp4096", "modp6144"];
            var exps = [0, 1, 2, 4, 8, 16, 32];
            var results = arithm.PGroup.benchFixExp(getPGroups(pGroupNames),
                                                    minSamples,
                                                    exps,
                                                    randomSource);
            var tableString =
                benchmark.grpIntTable("Exps", exps, pGroupNames, results);
            postMessage(["FixModPow.exp", tableString]);

        } else if (command == "ElGamal") {        
            var indices = getIndices(maxWidth);
            var pGroupNames = ["modp3072", "modp4096", "modp6144",
                               "P-256", "secp384r1", "P-521"];
            var results = crypto.ElGamal.benchEncrypt(true,
                                                      getPGroups(pGroupNames),
                                                      maxWidth,
                                                      minSamples,
                                                      randomSource,
                                                      statDist);
            var tableString = benchmark.grpIntTable("Width",
                                                    indices,
                                                    pGroupNames,
                                                    results);
            postMessage(["ElGamal", tableString]);

        } else if (command == "ElGamalZKPoKWriteIn") {
        
            var indices = getIndices(maxWidth);
            var pGroupNames = ["modp3072", "modp4096", "modp6144",
                               "P-256", "secp384r1", "P-521"];
            var results =
                crypto.ElGamalZKPoKWriteIn.benchEncrypt(true,
                                                        getPGroups(pGroupNames),
                                                        hashfunction,
                                                        maxWidth,
                                                        minSamples,
                                                        randomSource,
                                                        statDist);

            var tableString =
                benchmark.grpIntTable("Width", indices, pGroupNames, results);
            postMessage(["ElGamalZKPoKWriteIn", tableString]);

        } else if (command == "NaorYung") {
        
            var indices = getIndices(maxWidth);
            var pGroupNames = ["modp3072", "modp4096", "modp6144",
                               "P-256", "secp384r1", "P-521"];
            var results =
                crypto.ElGamalZKPoKWriteIn.benchEncrypt(false,
                                                        getPGroups(pGroupNames),
                                                        hashfunction,
                                                        maxWidth,
                                                        minSamples,
                                                        randomSource,
                                                        statDist);
            var tableString =
                benchmark.grpIntTable("Width", indices, pGroupNames, results);
            postMessage(["NaorYung", tableString]);
        } else {
            throw Error("Unknown command! (" + command + ")");
        }
    }
};

}

// This is in case of normal worker start
if (window != self) {
    bench_vjsc();
}
